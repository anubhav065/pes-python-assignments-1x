list_ = []

while True:
	if len(list_) < 10:
		print("Keep on adding elements for now")
		a = input("enter a number: ")
		list_.append(a)
	else:
		c = input("Minimum number of elemnts are in the list. Do you want to keep adding numbers? y/n: ")
		if c == 'y':
			a = input("enter a number: ")
			list_.append(a)
		else:
			break

print("Display all numbers")
for i in list_:
	print(i)

print("Slicing...")
print(list_[0:len(list_)//2])

print("Repeating numbers")
num = int(input("How many times do you want to repeat?"))
print(list_*num)

print("Creating new list to concatenate")
new_list = []
while True:
	c = input("Enter a number? or press q to quit entering a number")
	if c == 'q':
		break
	elif 48<=ord(c)<=57:
		new_list.append(c)
	else:
		print("Enter a valid option please...")

print("Concatenate two lists")
print(list_+new_list)
