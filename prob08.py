list_ = []

while True:
	if len(list_) < 10:
		print("Keep on adding elements for now")
		a = input("enter a number: ")
		list_.append(a)
	else:
		c = input("Minimum number of elemnts are in the list. Do you want to keep adding numbers? y/n: ")
		if c == 'y':
			a = input("enter a number: ")
			list_.append(a)
		else:
			break

tuple_ = tuple(list_)
print("Display all numbers")
for i in tuple_:
	print(i)

print("Slicing...")
print(tuple_[0:len(tuple_)//2])

print("Repeating numbers")
num = int(input("How many times do you want to repeat?"))
print(tuple_*num)

print("Creating new tuple to concatenate")
new_list = []
while True:
	c = input("Enter a number? or press q to quit entering a number")
	if c == 'q':
		break
	elif 48<=ord(c)<=57:
		new_list.append(c)
	else:
		print("Enter a valid option please...")

new_tuple = tuple(new_list)
print("Concatenate two tuples")
print(tuple_+new_tuple)
