try:
    terms = int(input("How many numbers of fibo series do you want to print?: "))
except ValueError:
    print("Please enter a valid number")
else:
    first = 0
    second = 1
    if terms == 1:
        print(first)
    elif terms == 2:
        print(first)
        print(second)
    elif terms > 2:
        print(first)
        print(second)
        while terms >= 3:
            next_ = first+second
            print(next_)
            first = second
            second = next_
            terms -= 1
            
try:
    limit = int(input("What's the maximum value upto which you want to print fibo series?: "))
except ValueError:
    print("Please enter a valid number")
else:
    first = 0
    second = 1
    if limit == 0:
        pass
    elif limit == 1:
        print(first)
    elif limit == 2:
        print(first)
        print(second)
    elif limit >= 3:
        print(first)
        print(second)
        next_ = first+second
        while next_ < limit:
            print(next_)
            first = second
            second = next_
            next_ = first+second