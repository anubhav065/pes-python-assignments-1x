def isPrime(number):
    i = 2
    while i < number:
        if number%i == 0:
            return 0
        i += 1
    if i == number:
        return 1
    else:
        return 0
    
try:
    number = int(input("Which number do you want to check for primality?"))
except ValueError:
    print("Please enter a number")
else:
    if(isPrime(number)):
        print("Yes, {} is a prime number".format(number))
    else:
        print("No, {} is not a prime number".format(number))
        
try:
    number = int(input("Enter the number upto which you want to display prime numbers"))
except ValueError:
    print("Please enter a number")
else:
    i = 2
    while i <= number:
        if(isPrime(i)):
            print(i)
        i += 1
        

    
