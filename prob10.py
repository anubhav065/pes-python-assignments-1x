def add(a, b):
	a += b
	print("sum is {}".format(a))

def sub(a, b):
	if a>=b:
		a -= b
		print("difference is {}".format(a))
	else:
		b -= a
		print("difference is {}".format(b))

def mul(a, b):
	a *= b
	print("Product is {}".format(a))

def int_div(a, b):
	try:
		a //= b
	except ZeroDivisionError:
		print("Can't divide by zero!")
	else:
		print("Integer Division: Quotient is {}".format(a))

def mod(a, b):
	try:
		a %= b
	except ZeroDivisionError:
		print("Can't divide by zero!")
	else:
		print("Remainder is {}".format(a))

def exponent(a, b):
	c,d = a,b
	a **= b
	print("{} to the power of {} is {}".format(c, d, a))

def floor_div(a, b):
	try:
		a /= b
	except ZeroDivisionError:
		print("Can't divide by zero!")
	else:
		print("Floor division: Quotient is {}".format(a))




try:
	num1 = int(input("Enter first number: "))
	num2 = int(input("Enter second number: "))
except ValueError:
	print("PLease make sure you are entering a number")
else:
	add(num1, num2)
	sub(num1, num2)
	mul(num1, num2)
	int_div(num1, num2)
	mod(num1, num2)
	exponent(num1, num2)
	floor_div(num1, num2)