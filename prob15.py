list_ = []

for i in range(0,5):
    a = input("Enter a name: ")
    list_.append(a)

name_ = input("Which name do you want to look for in the list?: ")

print("Using in operator")
if name_ in list_:
    print("yes, {} is in the list".format(name_))
else:
    print("no, {} is not in the list".format(name_))
    
print("Without using in operator")
i = 0

while i < len(list_):
    if name_ == list_[i]:
        print("yes, {} is in the list".format(name_))
        break
    i+=1
    
else:
    print("no, {} is not in the list".format(name_))
    
print("printing the list in reverse direction")
print(list_[::-1])