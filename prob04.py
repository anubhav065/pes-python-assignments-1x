def isPrime(number):
	i = 2
	while i < number:
		if number % i == 0:
			return False
		i+=1
	return True

try:
	num1 = int(input("Enter a number: "))
except ValueError:
	print("Please make sure you are entering a number")
else:
    if num1 < 2:
        print("Neither prime nor composite")
    else:
        if isPrime(num1):
            print("Prime")
        else:
            print("composite")