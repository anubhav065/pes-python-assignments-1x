A = []
B = []

while True:
    if len(A) < 10 and len(B) < 10:
        print("Please add data to the lists")
        emp_id = input("Enter employee ID: ")
        emp_name = input("Enter employee name: ")
        A.append(emp_id)
        B.append(emp_name)
    else:
        check = input("There is sufficicent data in the lists. Do you want to keep adding data? Press y/n")
        if check == 'n':
            break
        elif check == 'y':
            emp_id = input("Enter employee ID: ")
            emp_name = input("Enter employee name: ")
            A.append(emp_id)
            B.append(emp_name)
        else:
            print("Please enter valid option")
            
print("Displaying all names on screen")            
for i in B:
    print(i)
    
try:
    index = int(input("Please enter an index from where you want to read ID and name"))
except ValueError:
    print("Please enter a number")
except IndexError:
    print("Index is out of bounds")
else:
    print("ID is {} and Name is {}".format(A[index], B[index]))
    
print("Names between 4th and 9th position are: {}". format(B[4:10]))
print("Names between 3rd and last index are: {}". format(B[3:]))

try:
    num = int(input("How many times do you want to repeat the lists?"))
except ValueError:
    print("Please enter a number")
else:
    print(A*num)
    print(B*num)
    
print("Concatenating the lists...")
print(A+B)

print("Displaying list elements side by side...")
for i in range(len(A)):
    print(A[i], B[i])