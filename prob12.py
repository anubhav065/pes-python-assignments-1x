list_ = []

while len(list_) < 10:
	a = int(input("Enter a number"))
	list_.append(a)

sum_ = 0

for i in list_:
	sum_ += i

print("Average of given numbers is {}".format(sum_/10))

print("Numbers greater than average..")
for i in list_:
	if i > sum_/10:
		print(i)

print("Numbers lesser than average..")
for i in list_:
	if i < sum_/10:
		print(i)

print("Numbers equal to average..")
for i in list_:
	if i == sum_/10:
		print(i)		