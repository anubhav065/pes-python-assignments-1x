try:
	a = int(input("Enter first number: "))
	b = int(input("Enter second number: "))

except ValueError:
	print("PLease make sure you are entering a number")
else:
	c = a & b;        
	print("Line 1 - BITWISE AND of first and second number is ", c)

	c = a | b;         
	print("Line 2 - BITWISE OR of first and second number is ", c)

	c = a ^ b;        
	print("Line 3 - BITWISE XOR of first and second number is  is ", c)

	c = ~a;           
	print("Line 4 - Negation of first number is  ", c)

	c = a << 2;       
	print("Line 5 - Left shift operation on first number gives", c)

	c = a >> 2;       
	print("Line 6 - RIght shift operation on first number gives", c)