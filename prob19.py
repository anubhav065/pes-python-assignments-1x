print("using for loop")
for i in range(101):
    if i/10 < 5 and i%10 == 0:
        continue
    elif i/10 == 5 and i%10 == 0:
        break
    elif i%2!=0:
        continue
    else:
        print(i)
        
print("using while loop")
j = 0
while j <= 100:
    if (5 < j/10 < 9) and j%10 == 0:
        j += 1
        continue
    elif j/10 == 9 and j%10 == 0:
        break
    elif j%2!=0:
        j+=1
        continue
    else:
        print(j)
    j += 1