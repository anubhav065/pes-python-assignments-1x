list_ = []

while True:
    check = input("Do you want to enter a number in the list? Press y/n: ")
    if check == 'n':
        break
    elif check == 'y':
        number = int(input("Enter a number"))
        list_.append(number)
    else:
        print("Please enter a valid option")
        
print("Maximum number is {}".format(max(list_)))
print("Minimum number is {}".format(min(list_)))