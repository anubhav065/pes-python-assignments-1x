def add(a, b):
	print("sum is {}".format(a+b))

def sub(a, b):
	if a>=b:
		print("difference is {}".format(a-b))
	else:
		print("difference is {}".format(b-a))

def mul(a, b):
	print("Product is {}".format(a*b))

def div(a, b):
	try:
		c = a/b
	except ZeroDivisionError:
		print("Can't divide by zero!")
	else:
		print("Quotient is {}".format(c))
try:
	num1 = int(input("Enter first number: "))
	num2 = int(input("Enter second number: "))
except ValueError:
	print("PLease make sure you are entering a number")
else:
	add(num1, num2)
	sub(num1, num2)
	mul(num1, num2)
	div(num1, num2)
