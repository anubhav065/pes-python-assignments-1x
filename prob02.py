try:
	num1 = int(input("Enter first number: "))
	num2 = int(input("Enter second number: "))
	num3 = int(input("Enter third number: "))

except ValueError:
	print("PLease make sure you are entering a number")

else:
	if num1 > num2:
		if num1 > num3:
			print("Maximum number is {}".format(num1))
		else:
			print("Maximum number is {}".format(num3))
	elif num2 > num1:
		if num2 > num3:
			print("Maximum number is {}".format(num2))
		else:
			print("Maximum number is {}".format(num3))
	elif num1 == num3:
		print("All are equal")
	elif num3 > num1:
		print("Maximum number is {}".format(num3))
	else:
		print("Maximum number is {}".format(num1))

